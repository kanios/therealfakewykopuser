require_relative './../exception/bad_comment_entry_exception'

class CommentsBag

  def initialize
    @bag = Array.new
  end

  def add(hash)
    validate hash
    @bag << hash
  end

  def get_all
    @bag
  end

  private def validate(hash)
    if hash[:main].nil? || hash[:children].nil?
      raise BadCommentEntryException
    end
  end

end
