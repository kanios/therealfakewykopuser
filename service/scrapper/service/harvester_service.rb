require_relative '../util/content_preparator'

class HarvesterService

  def get_entries(source)
    source.css('.entry.iC')
  end

  def get_children(entry)
    result = Array.new
    entry.css('li').each do |li|
      pts = li.css('.author.ellipsis').css('p').css('b').text.to_i
      if pts === 0
        next
      end
      content = ContentPreparator::prepare li.css('.text').css('p').text
      result << {:pts => pts, :content => content}
    end
    result
  end

  def get_main(entry)
    ContentPreparator::prepare entry.css('div')[0].css('.text').css('p').text
  end

  def get_the_best(children)
    begin
      children.max_by{|k| k[:pts] }[:content]
    rescue StandardError
      nil
    end
  end

end