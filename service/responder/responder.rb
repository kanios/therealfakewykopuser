require_relative './../guardian/guardian'
require_relative './../scrapper/factory/scrapper_factory'
require_relative './../validator/simple_validator'


class Responder

  def initialize
    @scrapper = ScrapperFactory.create
    @validator = SimpleValidator.new
    @guardian = Guardian.new
  end

  def execute(thread)
    iterator = 1
    until false
      @scrapper.execute(iterator).get_all.each do |candidate|
        if @guardian.check(@validator.validate(thread, candidate[:main]))
          return candidate[:children]
        end
      end
      iterator += 1
    end
  end

end