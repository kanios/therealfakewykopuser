class ContentPreparator

  def self.prepare(content)
    content
      .gsub(/@(.*?):/, '')
      .gsub!(/\s+/, ' ')
      .gsub('. . . kliknij, aby rozwinąć obrazek . . .', '')
      .split('źródło: ')[0]
      .strip
  end

end