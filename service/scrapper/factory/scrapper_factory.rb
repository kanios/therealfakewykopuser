require_relative './../scrapper'
require_relative './../harvester'

class ScrapperFactory

  def self.create
    Scrapper.new(Harvester.new)
  end

end
