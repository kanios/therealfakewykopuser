class SimpleValidator

  def validate(comm, sentence)
    comm = comm.downcase
    sentence = sentence.downcase
    result = 0
    comm.split(' ').each do |word|
      unless sentence.index(word).nil?
        result += 1
      end
    end
    result.to_f / sentence.split(' ').length
  end

end