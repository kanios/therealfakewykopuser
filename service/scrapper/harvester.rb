require 'yaml'
require 'nokogiri'
require 'open-uri'

require_relative './value_object/comments_bag'
require_relative './service/harvester_service'

class Harvester

  def initialize
    @bag = CommentsBag.new
    @harvester_service = HarvesterService.new
    @url_prefix = YAML::load(File.open('./service/scrapper/config/wykop.yaml'))['prefix']
  end

  def harvest(page)
    url_to_scrap = sprintf(@url_prefix, page.to_s)
    source = Nokogiri::HTML open(url_to_scrap)
    entries = get_entries source
    entries.each do |entry|
      hash = {:main => get_main(entry), :children => get_the_best_content(get_children(entry))}
      begin
        @bag.add hash
      rescue StandardError
        next
      end
    end
    @bag
  end

  private def get_entries(source)
    @harvester_service.get_entries source
  end

  private def get_main(entry)
    @harvester_service.get_main entry
  end

  private def get_children(entry)
    @harvester_service.get_children entry
  end

  private def get_the_best_content(children)
    @harvester_service.get_the_best children
  end

end
