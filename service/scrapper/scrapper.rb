class Scrapper

  def initialize(harvester)
    @harvester = harvester
  end

  def execute(page)
    @harvester.harvest page
  end

end