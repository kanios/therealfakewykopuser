class BadCommentEntryException < StandardError

  def initialize(msg='Main comment or the best response is nil')
    super
  end

end