class Comment

  attr_accessor :content

  def initialize(content)
    @content = content
  end

  def validate
    if @content.nil?
      raise ArgumentError 'Comment content must not be empty'
    end
  end

end