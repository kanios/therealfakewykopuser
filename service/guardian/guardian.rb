require 'yaml'

class Guardian

  def initialize
    @to_go = YAML::load(File.open('./service/guardian/config/guardian.yaml'))['minimum_value'].to_f
  end

  def check(result)
    @to_go <= result
  end

end
